console.log('page-load - entered JS script section')

var submit_button = document.getElementById('send-button');
submit_button.onmouseup = get_form_info;

function get_form_info()
{
    var server_id = document.getElementById('select-server-address').value;
    console.log('selected server: ' + server_id);
    
    var port_num = document.getElementById('input-port-number').value;
    console.log('port number: ' + port_num);
    
    var request_types = document.getElementsByName('bsr-radios');
    for (i = 0; i < request_types.length; i++){
        if (request_types[i].checked){
            var request = request_types[i].value;
        }
    }
    console.log('selected request: ' + request);

    
    var key_check = document.getElementById('checkbox-use-key').checked;
    if (key_check){
        console.log('Use Key Box Checked');
        var key = document.getElementById('input-key').value;
        console.log('Key Entered: ' + key);
    }
    else{
        console.log('Not using key')
        var key = null;
    }

    var message_check = document.getElementById('checkbox-use-message').checked;
    if (message_check){
        console.log('Use Message Body Box Checked');
        var message_body = document.getElementById('text-message-body').value;
        console.log('Message body: ' + message_body);
    }
    else{
        console.log('Not using message')
        var message_body = null;
    }

    // localhost does not force http so must add it manually
    if (server_id == 'localhost'){
        var url = 'http://' + server_id + ':' + port_num + '/' + 'movies' +'/';
    }
    else{
        var url = server_id + ':' + port_num + '/' + 'movies' +'/';
    }
    

    make_request(url, request, key, message_body);
}

function make_request(url, request, key, message_body)
{
    if (key){
        url += key;
    }
    console.log('url making request to: ' + url);
    var xhr = new XMLHttpRequest();

    xhr.open(request, url, true);

    xhr.onload = function(e) {
        updateHTML(xhr.responseText);
		console.log(xhr.responseText);
	}
	
	xhr.onerror = function(e) {
		console.error(xhr.statusText);
    }
    
    if (message_body){
        xhr.send(message_body);
    }
    else{
        xhr.send(null);
    }

}

function updateHTML(text)
{
    var label1 = document.getElementById('answer-label');
    label1.innerHTML = text;
}

